import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PlacePageComponent} from './components/place-page/place-page.component';
import {AnnouncementListComponent} from './components/announcement-list/announcement-list.component';
import {MenuElementListComponent} from './components/menu-element-list/menu-element-list.component';
import {MemberListComponent} from './components/member-list/member-list.component';
import {FirebaseMessageListComponent} from './components/firebase-message-list/firebase-message-list.component';
import {ServedMealsListComponent} from './components/served-meals-list/served-meals-list.component';
import {MenuCategoryListComponent} from './components/menu-category-list/menu-category-list.component';
import {PlacesListComponent} from './components/places-list/places-list.component';
import {PaymentsComponent} from './components/payments/payments.component';
import {ProfileComponent} from './components/profile/profile.component';
import {LoginFormComponent} from './components/login-form/login-form.component';
import {MainScreenComponent} from './components/main-screen/main-screen.component';

const routes: Routes = [
  {path: 'login', component: LoginFormComponent},
  {
    path: 'user', component: MainScreenComponent, children: [
      {
        path: 'place/:placeId', children: [
          {path: 'mainPage', component: PlacePageComponent, pathMatch: 'full'},
          {path: 'announcement', component: AnnouncementListComponent, pathMatch: 'full'},
          {path: 'menuElement', component: MenuElementListComponent, pathMatch: 'full'},
          {path: 'members', component: MemberListComponent, pathMatch: 'full'},
          {path: 'messages', component: FirebaseMessageListComponent, pathMatch: 'full'},
          {path: 'servedMeals', component: ServedMealsListComponent, pathMatch: 'full'},
          {path: 'categoryList', component: MenuCategoryListComponent, pathMatch: 'full'},
          {path: '**', redirectTo: 'mainPage'}
        ]
      },
      {path: 'places', component: PlacesListComponent},
      {path: 'payments', component: PaymentsComponent},
      {path: 'profile', component: ProfileComponent},
      {path: '**', redirectTo: 'mainPage'}
    ]
  },
  {path: '**', redirectTo: 'login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
