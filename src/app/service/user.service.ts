import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {LoginResponseDto} from '../model/login-response-dto';
import {OAuth2Service} from './auth/o-auth2.service';
import {PlaceDto} from '../model/place-dto';
import {FirebaseMessageDto} from '../model/firebase-message-dto';
import {UserDto} from '../model/user-dto';
import {PlaceJoinCodeDto} from '../model/place-join-code-dto';
import {HttpConfiguration} from '../configuration/HttpConfiguration';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  HOST = HttpConfiguration.HOST;

  constructor(private http: HttpClient, private oAuth2Authenticator: OAuth2Service) {
  }

  passwordLogin(email: string, password: string, rememberMe: boolean): Observable<LoginResponseDto> {
    const body = new HttpParams()
      .set('username', email)
      .set('password', password)
      .set('grant_type', 'password');
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Basic bW9iaWxlLWFwcDpzZWNyZXQ=',
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    };
    return this.http.post(`${this.HOST}/oauth/token`, body, httpOptions)
      .pipe(map((value: any) => {
          const dto = LoginResponseDto.convert(value);
          this.oAuth2Authenticator.setLoginCredentials(dto.access_token, dto.refresh_token, dto.expires_in, rememberMe);
          return dto;
        }
      ));
  }

  refreshLogin(): Observable<LoginResponseDto> {
    const body = new HttpParams()
      .set('refresh_token', this.oAuth2Authenticator.getRefreshToken())
      .set('grant_type', 'refresh_token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Basic bW9iaWxlLWFwcDpzZWNyZXQ=',
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    };
    return this.http.post(`${this.HOST}/oauth/token`, body, httpOptions)
      .pipe(map((value: any) => {
          const dto = LoginResponseDto.convert(value);
          this.oAuth2Authenticator.setRefreshCredentials(dto.access_token, dto.refresh_token, dto.expires_in);
          return dto;
        }
      ));
  }

  getFirebaseMessages(placeId: string): Observable<FirebaseMessageDto[]> {
    return this.http.get(`${this.HOST}/firebaseMessage/${placeId}`)
      .pipe(map((value: any[]) => {
          return value.map(FirebaseMessageDto.convert);
        }
      ));
  }

  getUsersInPlace(placeId: string): Observable<UserDto[]> {
    return this.http.get(`${this.HOST}/user/fromPlace/${placeId}`)
      .pipe(map((value: any[]) => {
          return value.map(UserDto.convert);
        }
      ));
  }

  sendFirebaseNotification(placeId: string, title: string, priority: string, content: string) {
    const body = {title: title, content: content, level: priority};
    return this.http.post(`${this.HOST}/firebaseMessage/${placeId}`, body);
  }

  removeUserFromPlace(placeId: string, userId: string): Observable<any> {
    return this.http.delete(`${this.HOST}/place/${placeId}/deleteUser/${userId}`);
  }

  getUser(): Observable<UserDto> {
    return this.http.get(`${this.HOST}/user`)
      .pipe(map((value: any) => {
          return UserDto.convert(value);
        }
      ));
  }

  joinToPlace(placeId: string, joinToPlaceCode: string) {
    const body = {placeId: placeId, joinCode: joinToPlaceCode};
    return this.http.post(`${this.HOST}/place/joinToPlace`, body);
  }

}
