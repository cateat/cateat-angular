import {Injectable} from '@angular/core';
import {HttpConfiguration} from '../configuration/HttpConfiguration';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {ServedMealDto} from '../model/served-meal-dto';

@Injectable({
  providedIn: 'root'
})
export class ServedMealService {

  HOST = HttpConfiguration.HOST;

  constructor(private http: HttpClient) {
  }

  getAllServedMeals(placeId: string) {
    return this.http.get(`${this.HOST}/servedMeal/${placeId}`)
      .pipe(map((value: any[]) => value.map(it => ServedMealDto.convert(it))));
  }

  addServedElement(placeId: string, dateStarts: Date, dateEnds: Date, mealCategoryId: string, menuElements: string[]) {
    const body = {date: dateStarts, ends: dateEnds, mealDefinitionId: mealCategoryId, menuElements: menuElements};
    return this.http.post(`${this.HOST}/servedMeal/${placeId}`, body);
  }

  editServedElement(placeId: string, oldCategory: string, servedMealId: string, dateStarts: Date, dateEnds: Date, mealCategoryId: string,
                    menuElements: string[]) {
    const body = {id: servedMealId, date: dateStarts, ends: dateEnds, mealDefinitionId: mealCategoryId, menuElements: menuElements};
    console.log(body);
    return this.http.put(`${this.HOST}/servedMeal/${placeId}/${oldCategory}`, body);
  }

  removeElement(placeId: string, categoryId: string, servedMealId: string) {
    return this.http.delete(`${this.HOST}/servedMeal/${placeId}/${categoryId}/${servedMealId}`);
  }

}
