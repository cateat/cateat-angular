import {Injectable} from '@angular/core';
import {HttpConfiguration} from '../configuration/HttpConfiguration';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {MenuCategoryDto} from '../model/menu-category-dto';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MenuCategoryService {

  HOST = HttpConfiguration.HOST;

  constructor(private http: HttpClient) {
  }

  getAllMenuCategories(placeId: string): Observable<MenuCategoryDto[]> {
    return this.http.get(`${this.HOST}/category/${placeId}`)
      .pipe(map((value: any[]) => {
          console.log('category');
          console.log(value);
          return value.map(MenuCategoryDto.convert);
        }
      ));
  }

  addCategory(placeId: string, name: string, description: string, isActive: boolean): Observable<any> {
    const body = {name: name, description: description, active: isActive};
    return this.http.post(`${this.HOST}/category/${placeId}`, body);
  }

  editCategory(placeId: string, categoryId: string, name: string, description: string, isActive: boolean): Observable<any> {
    const body = {id: categoryId, name: name, description: description, active: isActive};
    return this.http.put(`${this.HOST}/category/${placeId}`, body);
  }

  deleteCategory(placeId: string, categoryId: string): Observable<any> {
    return this.http.delete(`${this.HOST}/category/${placeId}/${categoryId}`);
  }

}
