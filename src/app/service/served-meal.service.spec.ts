import { TestBed } from '@angular/core/testing';

import { ServedMealService } from './served-meal.service';

describe('ServedMealService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServedMealService = TestBed.get(ServedMealService);
    expect(service).toBeTruthy();
  });
});
