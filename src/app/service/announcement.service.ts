import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AnnouncementDto} from '../model/announcement-dto';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {HttpConfiguration} from '../configuration/HttpConfiguration';

@Injectable({
  providedIn: 'root'
})
export class AnnouncementService {

  HOST = HttpConfiguration.HOST;

  constructor(private http: HttpClient) {
  }

  getAnnouncements(placeId: string): Observable<AnnouncementDto[]> {
    return this.http.get(`${this.HOST}/announcement/${placeId}`)
      .pipe(map((value: any[]) => {
          return value.map(AnnouncementDto.convert);
        }
      ));
  }

  addAnnouncement(placeId: string, title: string, description: string, isActive: boolean): Observable<any> {
    const body = {name: title, description: description, isActive: isActive};
    return this.http.post(`${this.HOST}/announcement/${placeId}`, body);
  }

  editAnnouncement(placeId: string, announcementId: String, title: string, description: string, isActive: boolean): Observable<any> {
    const body = {id: announcementId, name: title, description: description, isActive: isActive};
    return this.http.put(`${this.HOST}/announcement/${placeId}`, body);
  }

  deleteAnnouncement(placeId: string, announcementId: String): Observable<any> {
    return this.http.delete(`${this.HOST}/announcement/${placeId}/${announcementId}`);
  }

}
