import { TestBed } from '@angular/core/testing';

import { MenuElementService } from './menu-element.service';

describe('MenuElementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MenuElementService = TestBed.get(MenuElementService);
    expect(service).toBeTruthy();
  });
});
