import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class OAuth2Service {

  private static readonly ACCESS_TOKEN = 'ACCESS_TOKEN';
  private static readonly REFRESH_TOKEN = 'REFRESH_TOKEN';
  private static readonly EXPIRES_ON = 'EXPIRES_ON';
  private static readonly REMEMBER_ME = 'REMEMBER_ME';
  private static readonly TRUE = 'true';

  private accessToken: string = null;
  private refreshToken: string = null;
  private expiresOn: Date = null;
  private rememberMe: boolean = null;

  private static setValueInStorage(key: string, value: string, inLocalStorage: boolean) {
    if (inLocalStorage === true) {
      localStorage.setItem(key, value);
    } else {
      sessionStorage.setItem(key, value);
    }
  }

  private static getValue(key: string): string {
    const fromLocalStorage = localStorage.getItem(key);
    if (fromLocalStorage !== null) {
      return fromLocalStorage;
    } else {
      return sessionStorage.getItem(key);
    }
  }

  private static clearValue(key: string) {
    localStorage.removeItem(key);
    sessionStorage.removeItem(key);
  }

  public getAccessToken(): string {
    this.protectNotLoadedFromStorage();
    return OAuth2Service.getValue(OAuth2Service.ACCESS_TOKEN);
  }

  public getRefreshToken(): string {
    this.protectNotLoadedFromStorage();
    return OAuth2Service.getValue(OAuth2Service.REFRESH_TOKEN);
  }

  public isAuthenticated(): boolean {
    this.protectNotLoadedFromStorage();
    console.log('now' + new Date().getTime());
    console.log('expiredOn' + this.expiresOn.getTime());
    return new Date() < this.expiresOn;
  }

  public clearData() {
    this.accessToken = null;
    this.refreshToken = null;
    this.expiresOn = null;
    this.rememberMe = null;
    OAuth2Service.clearValue(OAuth2Service.ACCESS_TOKEN);
    OAuth2Service.clearValue(OAuth2Service.REFRESH_TOKEN);
    OAuth2Service.clearValue(OAuth2Service.EXPIRES_ON);
    OAuth2Service.clearValue(OAuth2Service.REMEMBER_ME);
  }

  protectNotLoadedFromStorage() {
    if (this.accessToken === null) {
      this.loadAllFromStorage();
    }
  }

  loadAllFromStorage() {
    this.accessToken = OAuth2Service.getValue(OAuth2Service.ACCESS_TOKEN);
    this.refreshToken = OAuth2Service.getValue(OAuth2Service.REFRESH_TOKEN);
    this.expiresOn = new Date(Number(OAuth2Service.getValue(OAuth2Service.EXPIRES_ON)));
    this.rememberMe = OAuth2Service.getValue(OAuth2Service.REMEMBER_ME) === OAuth2Service.TRUE;
  }

  public setLoginCredentials(accessToken: string, refreshToken: string, expiresIn: number, rememberMe: boolean) {

    this.rememberMe = rememberMe;
    OAuth2Service.setValueInStorage(OAuth2Service.REMEMBER_ME, rememberMe ? 'true' : 'false', rememberMe);

    this.setRefreshCredentials(accessToken, refreshToken, expiresIn);
  }

  public setRefreshCredentials(accessToken: string, refreshToken: string, expiresIn: number) {
    this.accessToken = accessToken;
    OAuth2Service.setValueInStorage(OAuth2Service.ACCESS_TOKEN, accessToken, this.rememberMe);

    this.refreshToken = refreshToken;
    OAuth2Service.setValueInStorage(OAuth2Service.REFRESH_TOKEN, refreshToken, this.rememberMe);

    this.expiresOn = new Date();
    this.expiresOn.setSeconds(this.expiresOn.getSeconds() + expiresIn);
    OAuth2Service.setValueInStorage(OAuth2Service.EXPIRES_ON, this.expiresOn.getTime().toString(), this.rememberMe);
  }

}
