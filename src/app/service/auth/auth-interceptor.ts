import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {OAuth2Service} from './o-auth2.service';
import {UserService} from '../user.service';
import {catchError, filter, finalize, switchMap, take} from 'rxjs/operators';
import {LoginResponseDto} from '../../model/login-response-dto';
import {Router} from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(public auth: OAuth2Service, private userService: UserService, private router: Router) {
  }

  tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  isRefreshing = false;
  expiredTokens = [''];

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (request.headers.get('Authorization') == null) {

      if (!this.auth.isAuthenticated()) {
        return this.refreshToken(request, next);
      }

      return this.executeAuthorizedRequest(request, next)
    } else {
      return next.handle(request);
    }
  }

  executeAuthorizedRequest(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: {
        Authorization: `Bearer ${this.auth.getAccessToken()}`
      }
    });
    return next.handle(request);
  }

  refreshToken(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log(`refreshToken ${req.url}`);
    if (!this.isRefreshing) {
      this.isRefreshing = true;
      this.expiredTokens = this.expiredTokens.concat(this.auth.getAccessToken());
      return this.userService.refreshLogin()
        .pipe(switchMap((loginResponse: LoginResponseDto) => {
            this.auth.setRefreshCredentials(loginResponse.access_token, loginResponse.refresh_token, loginResponse.expires_in);
            this.tokenSubject.next(this.auth.getAccessToken());
            return this.executeAuthorizedRequest(req, next);
          }), catchError(error => {
            return this.logoutUser();
          }), finalize(() => this.isRefreshing = false)
        );
    } else {
      return this.tokenSubject
        .pipe(filter(token => !this.expiredTokens.find(v => v === token)))
        .pipe(take(1))
        .pipe(switchMap(token => {
          return this.executeAuthorizedRequest(req, next);
        }));
    }
  }

  logoutUser() {
    this.router.navigate(['']);
    return Observable.throw('');
  }

}
