import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {PlaceDto} from '../model/place-dto';
import {map} from 'rxjs/operators';
import {PlaceJoinCodeDto} from '../model/place-join-code-dto';
import {HttpConfiguration} from '../configuration/HttpConfiguration';
import {HttpClient} from '@angular/common/http';
import {OAuth2Service} from './auth/o-auth2.service';

@Injectable({
  providedIn: 'root'
})
export class PlaceService {

  HOST = HttpConfiguration.HOST;

  constructor(private http: HttpClient, private oAuth2Authenticator: OAuth2Service) {
  }

  getAllPlaces(): Observable<PlaceDto[]> {
    return this.http.get(`${this.HOST}/place`)
      .pipe(map((value: any[]) => {
          return value.map(PlaceDto.convert);
        }
      ));
  }

  getPlaceById(placeId: string): Observable<PlaceDto> {
    return this.http.get(`${this.HOST}/place/${placeId}`)
      .pipe(map((value: any) => {
          return PlaceDto.convert(value);
        }
      ));
  }

  getPlaceJoinCode(placeId: string): Observable<PlaceJoinCodeDto> {
    return this.http.get(`${this.HOST}/place/${placeId}/joinToPlaceCode`)
      .pipe(map((value: any) => {
          return PlaceJoinCodeDto.convert(value);
        }
      ));
  }

  changePlaceJoinCode(placeId: string): Observable<any> {
    return this.http.patch(`${this.HOST}/place/${placeId}/joinToPlaceCode`, {});
  }

  addPlace(placeModel: PlaceDto) {
    const body = {
      name: placeModel.name,
      description: placeModel.description,
      allergenStrategy: placeModel.allergenStrategy,
      address: placeModel.address,
      phoneNumber: placeModel.phoneNumber,
      email: placeModel.email,
      website: placeModel.website,
      facebookPage: placeModel.facebookPage,
      twitterPage: placeModel.twitterPage,
      pysznePlPage: placeModel.pysznePlPage,
      pizzaPortalPage: placeModel.pizzaPortalPage,
      active: placeModel.active
    };
    return this.http.post(`${this.HOST}/place`, body);
  }

  editPlace(placeModel: PlaceDto) {
    return this.http.put(`${this.HOST}/place`, placeModel);
  }

}
