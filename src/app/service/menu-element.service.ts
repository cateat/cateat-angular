import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {HttpConfiguration} from '../configuration/HttpConfiguration';
import {MenuElementDto} from '../model/menu-element-dto';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MenuElementService {

  HOST = HttpConfiguration.HOST;

  constructor(private http: HttpClient) {
  }

  getAllMenuElements(placeId: string): Observable<MenuElementDto[]> {
    return this.http.get(`${this.HOST}/menuElement/all/${placeId}`)
      .pipe(map((value: any[]) => {
          console.log(value);
          return value.map(MenuElementDto.convert);
        }
      ));
  }

  addMenuElement(placeId: string, name: string, description: string, price: string, allergens: string[]): Observable<any> {
    const body = {
      name: name,
      description: description,
      price: Number(price),
      allergens: allergens
    };

    return this.http.post(`${this.HOST}/menuElement/${placeId}`, body);
  }

  editMenuElement(placeId: string, id: string, name: string, description: string, price: string, allergens: string[]) {
    const body = {
      id: id,
      name: name,
      description: description,
      price: Number(price),
      allergens: allergens
    };

    return this.http.put(`${this.HOST}/menuElement/${placeId}`, body);
  }

}
