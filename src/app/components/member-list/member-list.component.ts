import {Component, OnInit} from '@angular/core';
import {UserDto} from '../../model/user-dto';
import {UserService} from '../../service/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {PlaceJoinCodeDto} from '../../model/place-join-code-dto';
import {PlaceService} from '../../service/place.service';

@Component({
  selector: 'app-member-list',
  templateUrl: './member-list.component.html',
  styleUrls: ['./member-list.component.css']
})
export class MemberListComponent implements OnInit {

  dataList: UserDto[] = [];
  joinCode = '';
  placeId = '';
  me: UserDto = null;

  constructor(private userService: UserService, private placeService: PlaceService,
              private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.placeId = this.route.snapshot.params.placeId;
    this.refreshList();
    this.refreshJoinToPlaceCode();
    this.getMe();
  }

  getMe() {
    this.userService.getUser()
      .subscribe((value: UserDto) => {
        this.me = value;
      }, error1 => {
        console.log(error1);
      });
  }

  refreshList() {
    this.userService.getUsersInPlace(this.placeId)
      .subscribe((value: UserDto[]) => {
        this.dataList = [...value];
      }, error1 => {
        console.log(error1);
      });
  }

  refreshJoinToPlaceCode() {
    const placeId = this.route.snapshot.params.placeId;
    this.placeService.getPlaceJoinCode(placeId)
      .subscribe((value: PlaceJoinCodeDto) => {
        this.joinCode = value.code;
      }, error1 => {
        console.log(error1);
      });
  }

  changeCode() {
    const placeId = this.route.snapshot.params.placeId;
    this.placeService.changePlaceJoinCode(placeId)
      .subscribe((value: PlaceJoinCodeDto) => {
        this.refreshJoinToPlaceCode();
      }, error1 => {
        console.log(error1);
      });
  }

  removeUserFromPlace(userId: string) {
    this.userService.removeUserFromPlace(this.placeId, userId)
      .subscribe((value: PlaceJoinCodeDto) => {
        if (this.me.id !== userId) {
          this.refreshList();
        } else {
          this.router.navigate(['user', 'places']);
        }
      }, error1 => {
        console.log(error1);
      });
  }

}
