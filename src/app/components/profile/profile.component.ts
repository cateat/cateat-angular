import {Component, OnInit} from '@angular/core';
import {UserService} from '../../service/user.service';
import {UserDto} from '../../model/user-dto';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  userModel = new UserDto('', '', '', '');

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.getUserData();
  }

  getUserData() {
    this.userService.getUser()
      .subscribe((value: UserDto) => {
        this.userModel = value;
      }, error1 => {
        console.log(error1);
      });
  }

}
