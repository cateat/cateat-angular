import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {PlaceDto} from '../../../model/place-dto';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PlaceService} from '../../../service/place.service';
import {AddressDto} from '../../../model/address-dto';

@Component({
  selector: 'app-place-edit',
  templateUrl: './place-edit.component.html',
  styleUrls: ['./place-edit.component.css']
})
export class PlaceEditComponent implements OnInit {

  @Output() refresh = new EventEmitter<any>();
  placeModel: PlaceDto = null;
  visible = false;
  validateForm: FormGroup;
  isButtonLoading = false;

  constructor(private fb: FormBuilder, private placeService: PlaceService) {
  }

  ngOnInit() {
    this.initCreate();
  }

  isCreating() {
    return this.placeModel == null;
  }

  initCreate() {
    this.validateForm = this.fb.group({
      name: [null, [Validators.required]],
      isActive: [null, [Validators.required]],
      description: [null],
      street: [null, [Validators.required]],
      number: [null, [Validators.required]],
      postalCode: [null, [Validators.required]],
      city: [null, [Validators.required]],
      phoneNumber: [null, [Validators.required]],
      email: [null, [Validators.required]],
      webSite: [null],
      facebook: [null],
      twitter: [null],
      pysznePl: [null],
      pizzaPortal: [null]
    });
  }

  initEdit() {
    this.validateForm = this.fb.group({
      name: [this.placeModel.name, [Validators.required]],
      isActive: [this.placeModel.active, [Validators.required]],
      description: [this.placeModel.description],
      street: [this.placeModel.address.street, [Validators.required]],
      number: [this.placeModel.address.number, [Validators.required]],
      postalCode: [this.placeModel.address.postalCode, [Validators.required]],
      city: [this.placeModel.address.city, [Validators.required]],
      phoneNumber: [this.placeModel.phoneNumber, [Validators.required]],
      email: [this.placeModel.email, [Validators.required]],
      webSite: [this.placeModel.website],
      facebook: [this.placeModel.facebookPage],
      twitter: [this.placeModel.twitterPage],
      pysznePl: [this.placeModel.pysznePlPage],
      pizzaPortal: [this.placeModel.pizzaPortalPage]
    });
  }

  createOpen() {
    this.placeModel = null;
    this.visible = true;
    this.initCreate();
  }

  editOpen(model: PlaceDto) {
    this.placeModel = model;
    this.visible = true;
    this.initEdit();
  }

  close() {
    this.visible = false;
  }

  submit() {
    for (const i in this.validateForm.controls) {
      if (i !== null) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }
    }
    if (this.validateForm.valid) {
      this.isButtonLoading = true;
      const name = this.validateForm.get('name').value;
      const isActive = this.validateForm.get('isActive').value;
      const description = this.validateForm.get('description').value;
      const street = this.validateForm.get('street').value;
      const number = this.validateForm.get('number').value;
      const postalCode = this.validateForm.get('postalCode').value;
      const city = this.validateForm.get('city').value;
      const phoneNumber = this.validateForm.get('phoneNumber').value;
      const email = this.validateForm.get('email').value;
      const webSite = this.validateForm.get('webSite').value;
      const facebook = this.validateForm.get('facebook').value;
      const twitter = this.validateForm.get('twitter').value;
      const pysznePl = this.validateForm.get('pysznePl').value;
      const pizzaPortal = this.validateForm.get('pizzaPortal').value;

      if (this.isCreating()) {
        const body = new PlaceDto(null, name, description, 'IN_MENU', isActive,
          new AddressDto(street, number, city, postalCode, 0, 0), phoneNumber, email, webSite, facebook,
          twitter, pysznePl, pizzaPortal);

        this.placeService.addPlace(body)
          .subscribe(value => {
            this.isButtonLoading = false;
            this.close();
            this.refresh.emit({});
          }, error1 => {
            console.log(error1);
          });
      } else {
        const body = new PlaceDto(this.placeModel.id, name, description, 'IN_MENU', isActive,
          new AddressDto(street, number, city, postalCode, 0, 0), phoneNumber, email, webSite, facebook,
          twitter, pysznePl, pizzaPortal);

        this.placeService.editPlace(body)
          .subscribe(value => {
            this.isButtonLoading = false;
            this.close();
            this.refresh.emit({});
          }, error1 => {
            console.log(error1);
          });
      }
    }
  }
}
