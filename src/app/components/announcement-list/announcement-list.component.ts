import {Component, OnInit} from '@angular/core';
import {AnnouncementDto} from '../../model/announcement-dto';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {AnnouncementService} from '../../service/announcement.service';

@Component({
  selector: 'app-announcement-list',
  templateUrl: './announcement-list.component.html',
  styleUrls: ['./announcement-list.component.css']
})
export class AnnouncementListComponent implements OnInit {

  dataList: AnnouncementDto[] = [];

  constructor(private announcementService: AnnouncementService, private route: ActivatedRoute, private fb: FormBuilder) {
  }

  ngOnInit() {
    this.refreshList();
  }

  refreshList() {
    const placeId = this.route.snapshot.params.placeId;
    this.announcementService.getAnnouncements(placeId)
      .subscribe((value: AnnouncementDto[]) => {
        console.log('Result announcement');
        console.log(value);
        this.dataList = [...value];
      }, error1 => {
        console.log(error1);
      });
  }

  removeAnnouncement(announcementId: string) {
    const placeId = this.route.snapshot.params.placeId;
    this.announcementService.deleteAnnouncement(placeId, announcementId)
      .subscribe((value: AnnouncementDto[]) => {
        this.refreshList();
      }, error1 => {
        console.log(error1);
      });
  }

}
