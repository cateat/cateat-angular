import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {AnnouncementService} from '../../../service/announcement.service';
import {AnnouncementDto} from '../../../model/announcement-dto';

@Component({
  selector: 'app-announcement-edit-drawer',
  templateUrl: './announcement-edit-drawer.component.html',
  styleUrls: ['./announcement-edit-drawer.component.css']
})
export class AnnouncementEditDrawerComponent implements OnInit {

  @Output() refresh = new EventEmitter<any>();

  constructor(private announcementService: AnnouncementService, private fb: FormBuilder, private route: ActivatedRoute) {
  }

  announcementModel: AnnouncementDto = null;
  validateForm: FormGroup;
  visible = false;
  isButtonLoading = false;

  ngOnInit() {
    this.initCreate();
  }

  initCreate() {
    this.validateForm = this.fb.group({
      title: [null, [Validators.required]],
      content: [null, [Validators.required]],
      isActive: [true]
    });
  }

  initEdit() {
    this.validateForm = this.fb.group({
      title: [this.announcementModel.name, [Validators.required]],
      content: [this.announcementModel.description, [Validators.required]],
      isActive: [this.announcementModel.isActive]
    });
  }

  open(): void {
    this.visible = true;
  }

  openWithModel(announcementModel: AnnouncementDto) {
    this.announcementModel = announcementModel;
    this.open();
    this.initEdit();
  }

  close(): void {
    this.visible = false;
  }

  isCreating() {
    return this.announcementModel == null;
  }

  addAnnouncement() {
    for (const i in this.validateForm.controls) {
      if (i !== null) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }
    }
    if (this.validateForm.valid) {
      this.isButtonLoading = true;
      const title = this.validateForm.get('title').value;
      const content = this.validateForm.get('content').value;
      const isActive = this.validateForm.get('isActive').value;
      console.log(title + content + isActive);
      const placeId = this.route.snapshot.params.placeId;
      if (this.isCreating()) {
        this.announcementService.addAnnouncement(placeId, title, content, isActive)
          .subscribe(value => {
            this.isButtonLoading = false;
            this.close();
            this.refresh.emit({});
          }, error1 => {
            console.log(error1);
          });
      } else {
        this.announcementService.editAnnouncement(placeId, this.announcementModel.id, title, content, isActive)
          .subscribe(value => {
            this.isButtonLoading = false;
            this.close();
            this.refresh.emit({});
          }, error1 => {
            console.log(error1);
          });
      }
    }
  }

}
