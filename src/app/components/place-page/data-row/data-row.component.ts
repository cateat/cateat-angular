import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-data-row',
  templateUrl: './data-row.component.html',
  styleUrls: ['./data-row.component.css']
})
export class DataRowComponent implements OnInit {

  @Input() name: string;
  @Input() value: string;

  constructor() { }

  ngOnInit() {
  }

}
