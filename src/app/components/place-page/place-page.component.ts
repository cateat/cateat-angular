import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PlaceDto} from '../../model/place-dto';
import {AddressDto} from '../../model/address-dto';
import {PlaceService} from '../../service/place.service';

@Component({
  selector: 'app-place-page',
  templateUrl: './place-page.component.html',
  styleUrls: ['./place-page.component.css']
})
export class PlacePageComponent implements OnInit {

  placeModel: PlaceDto = new PlaceDto('', '', '', '', true,
    new AddressDto('', '', '', '', 0, 0),
    '', '', '', '', '', '', '');

  constructor(private placeService: PlaceService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.getPlace();
  }

  getPlace() {
    const placeId = this.route.snapshot.params.placeId;
    this.placeService.getPlaceById(placeId)
      .subscribe((value: PlaceDto) => {
        this.placeModel = value;
      }, error1 => {
        console.log(error1);
      });
  }

  getLabel(value: string) {
    return value == null ? 'Brak' : value;
  }

}
