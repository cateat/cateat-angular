import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MenuCategoryDto} from '../../model/menu-category-dto';
import {MenuCategoryService} from '../../service/menu-category.service';

@Component({
  selector: 'app-menu-category-list',
  templateUrl: './menu-category-list.component.html',
  styleUrls: ['./menu-category-list.component.css']
})
export class MenuCategoryListComponent implements OnInit {

  dataList: MenuCategoryDto[] = [];

  constructor(private menuCategoryService: MenuCategoryService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.refreshList();
  }

  refreshList() {
    this.menuCategoryService.getAllMenuCategories(this.route.snapshot.params.placeId)
      .subscribe((value: MenuCategoryDto[]) => {
        this.dataList = [...value];
      }, error1 => {
        console.log(error1);
      });
  }

  deleteCategory(categoryId: string) {
    this.menuCategoryService.deleteCategory(this.route.snapshot.params.placeId, categoryId)
      .subscribe((value: MenuCategoryDto[]) => {
        this.refreshList();
      }, error1 => {
        console.log(error1);
      });
  }

}
