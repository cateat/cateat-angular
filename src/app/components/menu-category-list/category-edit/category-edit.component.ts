import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MenuCategoryDto} from '../../../model/menu-category-dto';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {MenuCategoryService} from '../../../service/menu-category.service';

@Component({
  selector: 'app-category-edit',
  templateUrl: './category-edit.component.html',
  styleUrls: ['./category-edit.component.css']
})
export class CategoryEditComponent implements OnInit {

  @Output() refresh = new EventEmitter<any>();
  visible = false;
  model: MenuCategoryDto = null;
  isButtonLoading = false;
  validateForm: FormGroup;

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private menuCategoryService: MenuCategoryService) {
  }

  ngOnInit() {
    this.initCreate();
  }

  initCreate() {
    this.validateForm = this.fb.group({
      name: [null, [Validators.required]],
      description: [null],
      isActive: [true]
    });
  }

  initEdit() {
    this.validateForm = this.fb.group({
      name: [this.model.name, [Validators.required]],
      description: [this.model.description],
      isActive: [this.model.active]
    });
  }

  open() {
    this.model = null;
    this.visible = true;
    this.initCreate();
  }

  openEdit(model: MenuCategoryDto) {
    this.model = model;
    this.visible = true;
    this.initEdit();
  }

  close() {
    this.visible = false;
  }

  isCreating() {
    return this.model == null;
  }

  submit() {
    for (const i in this.validateForm.controls) {
      if (i !== null) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }
    }
    if (this.validateForm.valid) {
      this.isButtonLoading = true;
      const name = this.validateForm.get('name').value;
      const description = this.validateForm.get('description').value;
      const isActive = this.validateForm.get('isActive').value;
      const placeId = this.route.snapshot.params.placeId;
      if (this.isCreating()) {
        this.menuCategoryService.addCategory(placeId, name, description, isActive)
          .subscribe(value => {
            this.isButtonLoading = false;
            this.close();
            this.refresh.emit({});
          }, error1 => {
            console.log(error1);
          });
      } else {
        this.menuCategoryService.editCategory(placeId, this.model.id, name, description, isActive)
          .subscribe(value => {
            this.isButtonLoading = false;
            this.close();
            this.refresh.emit({});
          }, error1 => {
            console.log(error1);
          });
      }
    }
  }

}
