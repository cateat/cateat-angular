import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PlaceService} from '../../service/place.service';

@Component({
  selector: 'app-breadcrumb-bar',
  templateUrl: './breadcrumb-bar.component.html',
  styleUrls: ['./breadcrumb-bar.component.css']
})
export class BreadcrumbBarComponent implements OnInit {

  breadcrumbValued: string[] = [];

  constructor(private router: ActivatedRoute, private placeService: PlaceService) {
  }

  ngOnInit() {
    console.log('BreadcrumbBarComponent');
    this.router.url.subscribe(() => {
      this.analyseSnapshot();
    });
    this.analyseSnapshot();
  }

  analyseSnapshot() {
    console.log('BreadcrumbBarComponent -> analyseSnapshot');
    console.log(this.router.snapshot.firstChild);
    if (this.router.snapshot.firstChild.url[0].path === 'place') {
      const screenType = this.router.snapshot.firstChild.firstChild.url[0].path;
      const placeId = this.router.snapshot.firstChild.params.placeId;
      let screenName = '';
      console.log('LLLLL' + screenType);
      if (screenType === 'mainPage') {
        screenName = 'Strona główna';
      } else if (screenType === 'announcement') {
        screenName = 'Ogłoszenia';
      } else if (screenType === 'categoryList') {
        screenName = 'Etykiety menu';
      } else if (screenType === 'servedMeals') {
        screenName = 'Menu na okres';
      } else if (screenType === 'menuElement') {
        screenName = 'Pozycje menu';
      } else if (screenType === 'members') {
        screenName = 'Członkowie';
      } else if (screenType === 'messages') {
        screenName = 'Wiadomości';
      } else {
        screenName = 'Inne';
      }
      const oldPlaceValue = this.breadcrumbValued.length > 0 ? this.breadcrumbValued[0] : '';
      this.breadcrumbValued = [oldPlaceValue, screenName];
      this.getPlaceName(placeId, 0);
    } else if (this.router.snapshot.firstChild.url[0].path === 'places') {
      this.breadcrumbValued = ['Miejsca'];
    } else if (this.router.snapshot.firstChild.url[0].path === 'payments') {
      this.breadcrumbValued = ['Płatności'];
    } else if (this.router.snapshot.firstChild.url[0].path === 'profile') {
      this.breadcrumbValued = ['Profil'];
    } else {
      this.breadcrumbValued = ['Inne'];
    }
  }

  getPlaceName(placeId: string, index: number) {
    this.placeService.getPlaceById(placeId).subscribe(
      value => {
        console.log(value);
        this.breadcrumbValued[index] = value.name;
      }, error1 => {
        console.log(error1);
        this.breadcrumbValued[index] = '*';
      }
    );
  }

}
