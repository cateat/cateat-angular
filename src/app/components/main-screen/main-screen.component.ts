import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-main-screen',
  templateUrl: './main-screen.component.html',
  styleUrls: ['./main-screen.component.css']
})
export class MainScreenComponent implements OnInit {

  screenType = 'main';

  constructor(private router: Router) {
  }

  ngOnInit() {

    this.analyseUrl(this.router.url);

    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.analyseUrl(event.url);
      }
    });
  }

  analyseUrl(url: string) {
    console.log(url.split('/'));
    const tags = url.split('/');
    if (tags[0] === '' && tags[1] === 'user' && tags[2] === 'place') {
      this.screenType = 'place';
    } else {
      this.screenType = '';
    }
  }

}
