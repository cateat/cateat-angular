import {Component, OnInit} from '@angular/core';
import {MenuElementDto} from '../../model/menu-element-dto';
import {ActivatedRoute} from '@angular/router';
import {Allergens} from './Allergens';
import {MenuElementService} from '../../service/menu-element.service';

@Component({
  selector: 'app-menu-element-list',
  templateUrl: './menu-element-list.component.html',
  styleUrls: ['./menu-element-list.component.css']
})
export class MenuElementListComponent implements OnInit {

  dataList: MenuElementDto[] = [];

  constructor(private menuElementService: MenuElementService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.refreshElements();
  }

  refreshElements() {
    this.menuElementService.getAllMenuElements(this.route.snapshot.params.placeId)
      .subscribe((value: MenuElementDto[]) => {
        this.dataList = [...value];
      }, error1 => {
        console.log(error1);
      });
  }

  getAllergensString(allergensKey: string[]) {
    return allergensKey.map(value => Allergens.listOfAllergens.find(it => it.key === value).name).join(', ');
  }

}
