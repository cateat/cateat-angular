import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuElementListComponent } from './menu-element-list.component';

describe('MenuElementListComponent', () => {
  let component: MenuElementListComponent;
  let fixture: ComponentFixture<MenuElementListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuElementListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuElementListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
