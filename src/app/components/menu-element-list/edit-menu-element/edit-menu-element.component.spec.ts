import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMenuElementComponent } from './edit-menu-element.component';

describe('EditMenuElementComponent', () => {
  let component: EditMenuElementComponent;
  let fixture: ComponentFixture<EditMenuElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMenuElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMenuElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
