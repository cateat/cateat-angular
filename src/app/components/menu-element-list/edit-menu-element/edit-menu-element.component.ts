import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {MenuElementDto} from '../../../model/menu-element-dto';
import {MenuElementService} from '../../../service/menu-element.service';
import {Allergens} from '../Allergens';

@Component({
  selector: 'app-edit-menu-element',
  templateUrl: './edit-menu-element.component.html',
  styleUrls: ['./edit-menu-element.component.css']
})
export class EditMenuElementComponent implements OnInit {

  menuElement: MenuElementDto = null;
  listOfAllergens = Allergens.listOfAllergens;
  @Output('refresh') refreshEmitter = new EventEmitter<any>();

  constructor(private menuElementService: MenuElementService, private fb: FormBuilder, private route: ActivatedRoute) {
  }

  validateForm: FormGroup;
  visible = false;
  isButtonLoading = false;

  ngOnInit() {
    this.initCreateForm();
  }

  isElementCreating() {
    return this.menuElement == null;
  }

  public open(): void {
    this.visible = true;
    this.menuElement = null;
    this.initForm();
  }

  openWithModel(element: MenuElementDto): void {
    this.menuElement = element;
    this.visible = true;
    this.initForm();
  }

  initForm() {
    if (this.isElementCreating()) {
      this.initCreateForm();
    } else {
      this.initEditForm();
    }
  }

  initCreateForm() {
    this.validateForm = this.fb.group({
      name: [null, [Validators.required]],
      description: [null, [Validators.required]],
      price: [null],
      isActive: [true],
      allergens: [null]
    });
  }

  initEditForm() {
    console.log('initEditForm');
    console.log(this.menuElement);
    this.validateForm = this.fb.group({
      name: [this.menuElement.name as string, [Validators.required]],
      description: [this.menuElement.description, [Validators.required]],
      price: [this.menuElement.price],
      isActive: [this.menuElement.deleted],
      allergens: [this.menuElement.allergens]
    });
  }

  close(): void {
    this.visible = false;
  }

  submitMenuElement() {
    for (const i in this.validateForm.controls) {
      if (i !== null) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }
    }
    if (this.validateForm.valid) {
      this.isButtonLoading = true;
      const name = this.validateForm.get('name').value;
      const description = this.validateForm.get('description').value;
      const price = this.validateForm.get('price').value;
      const isActive = this.validateForm.get('isActive').value;
      const allergens = this.validateForm.get('allergens').value == null ? [] : this.validateForm.get('allergens').value;
      const placeId = this.route.snapshot.params.placeId;
      if (this.isElementCreating()) {
        this.menuElementService.addMenuElement(placeId, name, description, price, allergens)
          .subscribe(value => {
            this.isButtonLoading = false;
            this.close();
            this.refreshEmitter.emit({});
          }, error1 => {
            console.log(error1);
          });
      } else {
        this.menuElementService.editMenuElement(placeId, this.menuElement.id, name, description, price, allergens)
          .subscribe(value => {
            this.isButtonLoading = false;
            this.close();
            this.refreshEmitter.emit({});
          }, error1 => {
            this.isButtonLoading = false;
            console.log(error1);
          });
      }
    }
  }

}
