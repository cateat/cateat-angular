export class Allergens {
  public static readonly listOfAllergens = [
    {key: 'SULFUR', name: 'Dwutlenek siarki'},
    {key: 'EGGS', name: 'Jajka'},
    {key: 'LUPINE', name: 'Łubin'},
    {key: 'MOLLUSCS', name: 'Mięczaki'},
    {key: 'MILK', name: 'Mleko'},
    {key: 'MUSTARD', name: 'Musztarda'},
    {key: 'NUTS', name: 'Orzechy'},
    {key: 'PEANUTS', name: 'Orzechy ziemne'},
    {key: 'FISHES', name: 'Ryby'},
    {key: 'CELERY', name: 'Seler'},
    {key: 'CRUSTACEANS', name: 'Skorupiaki'},
    {key: 'SOY', name: 'Soja'},
    {key: 'GLUTEN', name: 'Zboża zawierające gluten'},
    {key: 'SESAME', name: 'Ziarna sezamu'}
  ];
}
