import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaceSiderMenuComponent } from './place-sider-menu.component';

describe('PlaceSiderMenuComponent', () => {
  let component: PlaceSiderMenuComponent;
  let fixture: ComponentFixture<PlaceSiderMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceSiderMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaceSiderMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
