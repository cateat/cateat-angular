import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-place-sider-menu',
  templateUrl: './place-sider-menu.component.html',
  styleUrls: ['./place-sider-menu.component.css']
})
export class PlaceSiderMenuComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    console.log(this.route.snapshot.firstChild.params);
  }

  getPlaceId(): string {
    return this.route.snapshot.firstChild.params.placeId;
  }

  openPage(pageName: string) {
    this.router.navigate(['user', 'place', this.getPlaceId(), pageName]);
  }

}
