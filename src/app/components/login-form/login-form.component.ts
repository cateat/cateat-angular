import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../service/user.service';
import {LoginResponseDto} from '../../model/login-response-dto';
import {NzMessageService} from 'ng-zorro-antd';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  validateForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private nzMessageService: NzMessageService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      userName: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true]
    });
  }

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      if (i !== null) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }
    }
    if (this.validateForm.valid) {
      const email = this.validateForm.get('userName').value;
      const password = this.validateForm.get('password').value;
      const remember = this.validateForm.get('remember').value;

      this.userService.passwordLogin(email, password, remember)
        .subscribe((response: LoginResponseDto) => {
          console.log(response);
          this.nzMessageService.success('Zalogowano pomyślnie');
          this.router.navigate(['user', 'places']);
        }, (error: any) => {
          console.log(error);
        });
    }
  }

}
