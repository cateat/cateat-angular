import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {OAuth2Service} from '../../service/auth/o-auth2.service';

@Component({
  selector: 'app-logged-header',
  templateUrl: './logged-header.component.html',
  styleUrls: ['./logged-header.component.css']
})
export class LoggedHeaderComponent implements OnInit {

  selectedIndex = -1;
  startFromThis = false;

  constructor(private router: Router, private route: ActivatedRoute, private oAuthService: OAuth2Service) {
  }

  ngOnInit() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        console.log(event);
        if (this.startFromThis) {
          this.startFromThis = false;
        } else {
          this.selectedIndex = -2;
        }
      }
    });
  }

  selectPlacesList(i) {
    this.selectedIndex = i;
    this.startFromThis = true;
  }

  logout() {
    this.oAuthService.clearData();
    this.selectedIndex = 3;
    this.startFromThis = true;
    // this.selectedIndex = -1;
  }

}
