import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServedMealEditComponent } from './served-meal-edit.component';

describe('ServedMealEditComponent', () => {
  let component: ServedMealEditComponent;
  let fixture: ComponentFixture<ServedMealEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServedMealEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServedMealEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
