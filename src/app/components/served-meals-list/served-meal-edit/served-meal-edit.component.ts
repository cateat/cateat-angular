import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MenuCategoryDto} from '../../../model/menu-category-dto';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {MenuCategoryService} from '../../../service/menu-category.service';
import {MenuElementService} from '../../../service/menu-element.service';
import {MenuElementDto} from '../../../model/menu-element-dto';
import {ServedMealService} from '../../../service/served-meal.service';
import {ServedMealDto} from '../../../model/served-meal-dto';

@Component({
  selector: 'app-served-meal-edit',
  templateUrl: './served-meal-edit.component.html',
  styleUrls: ['./served-meal-edit.component.css']
})
export class ServedMealEditComponent implements OnInit {

  @Output() refresh = new EventEmitter<any>();
  visible = false;
  model: ServedMealDto = null;
  isButtonLoading = false;
  validateForm: FormGroup;
  timeVariant = '';

  categories: MenuCategoryDto[] = [];
  menuElements: MenuElementDto[] = [];

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private menuCategoryService: MenuCategoryService,
              private menuElementService: MenuElementService, private servedMealService: ServedMealService) {
  }

  refreshData() {
    console.log('refreshData');
    const placeId = this.route.snapshot.params.placeId;
    let categoriesRefreshed = false;
    let elementsRefreshed = false;

    this.menuCategoryService.getAllMenuCategories(placeId)
      .subscribe(value => {
        console.log('getAllMenuCategories');
        this.categories = [...value];
        if (elementsRefreshed) {
          this.initForm();
        } else {
          categoriesRefreshed = true;
        }
      }, error1 => {
        console.log(error1);
      });

    this.menuElementService.getAllMenuElements(placeId)
      .subscribe(value => {
        console.log('getAllMenuElements');
        this.menuElements = [...value];
        if (categoriesRefreshed) {
          this.initForm();
        } else {
          elementsRefreshed = true;
        }
      }, error1 => {
        console.log(error1);
      });
  }

  initForm() {
    console.log('initForm');
    if (this.isCreating()) {
      this.initCreate();
    } else {
      this.initEdit();
    }
  }

  ngOnInit() {
    this.initForm();
    this.refreshData();
  }

  initCreate() {
    this.timeVariant = 'ONE_DAY';
    this.validateForm = this.fb.group({
      categoryId: [null, [Validators.required]],
      timeVariant: [this.timeVariant, [Validators.required]],
      timeSingle: [null],
      timeRange: [null],
      menuElements: [null, [Validators.required]]
    });
  }

  initEdit() {
    console.log('initEdit');
    const isOneDay = this.model.ends === null;
    console.log(isOneDay);
    this.timeVariant = isOneDay ? 'ONE_DAY' : 'MORE_THAN_ONE_DAY';
    console.log(this.timeVariant);
    const timeInput = isOneDay ? this.model.date : [this.model.date, this.model.ends];
    console.log(timeInput);
    this.validateForm = this.fb.group({
      categoryId: [this.model.mealCategory.id, [Validators.required]],
      timeVariant: [this.timeVariant, [Validators.required]],
      timeSingle: [this.model.date],
      timeRange: [isOneDay ? null : timeInput],
      menuElements: [this.model.menuElements.map(it => it.id), [Validators.required]]
    });
  }

  open() {
    this.model = null;
    this.visible = true;
    this.refreshData();
  }

  openEdit(model: ServedMealDto) {
    console.log('openEdit');
    console.log(model);
    this.model = model;
    this.visible = true;
    this.refreshData();
  }

  openCopy(model: ServedMealDto) {
    const newModel = new ServedMealDto(null, model.date, model.ends, model.mealCategory, model.menuElements);
    this.openEdit(newModel);
  }

  close() {
    this.visible = false;
  }

  isCreating() {
    return this.model == null;
  }

  isCopying() {
    return this.model != null && this.model.id == null;
  }

  isEditing() {
    return this.model != null && this.model.id != null;
  }

  submit() {
    for (const i in this.validateForm.controls) {
      if (i !== null) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }
    }
    if (this.validateForm.valid) {
      this.isButtonLoading = true;
      const categoryId = this.validateForm.get('categoryId').value;
      const menuElements = this.validateForm.get('menuElements').value;
      const timeSingle = this.validateForm.get('timeSingle').value;
      const timeRange = this.validateForm.get('timeRange').value;
      const isOneDay = this.validateForm.get('timeVariant').value === 'ONE_DAY';
      const placeId = this.route.snapshot.params.placeId;
      const dateStarts = isOneDay ? timeSingle : timeRange[0];
      const dateEnds = isOneDay ? null : timeRange[1];

      if (this.isCreating() || this.isCopying()) {
        this.servedMealService.addServedElement(placeId, dateStarts, dateEnds, categoryId, menuElements)
          .subscribe(value => {
            this.isButtonLoading = false;
            this.close();
            this.refresh.emit({});
          }, error1 => {
            console.log(error1);
          });
      } else {
        this.servedMealService.editServedElement(placeId, this.model.mealCategory.id, this.model.id, dateStarts, dateEnds, categoryId,
          menuElements)
          .subscribe(value => {
            this.isButtonLoading = false;
            this.close();
            this.refresh.emit({});
          }, error1 => {
            console.log(error1);
          });
      }
    }
  }


}
