import {Component, OnInit} from '@angular/core';
import {ServedMealDto} from '../../model/served-meal-dto';
import {ServedMealService} from '../../service/served-meal.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-served-meals-list',
  templateUrl: './served-meals-list.component.html',
  styleUrls: ['./served-meals-list.component.css']
})
export class ServedMealsListComponent implements OnInit {

  dataList: ServedMealDto[] = [];

  constructor(private servedMealService: ServedMealService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.refreshList();
  }

  refreshList() {
    const placeId = this.route.snapshot.params.placeId;
    this.servedMealService.getAllServedMeals(placeId)
      .subscribe((value: ServedMealDto[]) => {
        console.log('servedMealService');
        console.log(value);
        this.dataList = [...value];
      }, error1 => {
        console.log(error1);
      });
  }

  removeElement(categoryId: string, servedMealId: string) {
    const placeId = this.route.snapshot.params.placeId;
    this.servedMealService.removeElement(placeId, categoryId, servedMealId)
      .subscribe((value: ServedMealDto[]) => {
        this.refreshList();
      }, error1 => {
        console.log(error1);
      });
  }

}
