import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServedMealsListComponent } from './served-meals-list.component';

describe('ServedMealsListComponent', () => {
  let component: ServedMealsListComponent;
  let fixture: ComponentFixture<ServedMealsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServedMealsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServedMealsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
