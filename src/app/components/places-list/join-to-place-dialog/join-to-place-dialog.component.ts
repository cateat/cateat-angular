import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../../service/user.service';

@Component({
  selector: 'app-join-to-place-dialog',
  templateUrl: './join-to-place-dialog.component.html',
  styleUrls: ['./join-to-place-dialog.component.css']
})
export class JoinToPlaceDialogComponent implements OnInit {

  @Output() refresh = new EventEmitter<any>();
  isVisible = false;
  validateForm: FormGroup;
  isButtonLoading = false;

  constructor(private fb: FormBuilder, private userService: UserService) {
  }

  ngOnInit() {
    this.validateForm = this.fb.group({
      placeId: [null, [Validators.required]],
      placeKey: [null, [Validators.required]],
    });
  }

  show() {
    this.isVisible = true;
    this.reset();
  }

  close() {
    this.isVisible = false;
  }

  reset() {
    this.validateForm.reset();
  }

  handleCancel() {
    this.reset();
    this.close();
  }

  handleOk() {
    for (const i in this.validateForm.controls) {
      if (i !== null) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }
    }
    if (this.validateForm.valid) {
      this.isButtonLoading = true;
      const placeId = this.validateForm.get('placeId').value;
      const placeKey = this.validateForm.get('placeKey').value;
      this.userService.joinToPlace(placeId, placeKey)
        .subscribe(value => {
          this.reset();
          this.isButtonLoading = false;
          this.close();
        }, error1 => {
          console.log(error1);
          this.isButtonLoading = false;
        });
    }
  }

}
