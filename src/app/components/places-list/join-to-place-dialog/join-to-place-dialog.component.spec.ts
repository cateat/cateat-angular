import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinToPlaceDialogComponent } from './join-to-place-dialog.component';

describe('JoinToPlaceDialogComponent', () => {
  let component: JoinToPlaceDialogComponent;
  let fixture: ComponentFixture<JoinToPlaceDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoinToPlaceDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinToPlaceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
