import {Component, OnInit} from '@angular/core';
import {PlaceDto} from '../../model/place-dto';
import {Router} from '@angular/router';
import {PlaceService} from '../../service/place.service';

@Component({
  selector: 'app-places-list',
  templateUrl: './places-list.component.html',
  styleUrls: ['./places-list.component.css']
})
export class PlacesListComponent implements OnInit {

  places: PlaceDto[] = [];

  constructor(private placeService: PlaceService, private router: Router) {
  }

  ngOnInit() {
    this.refreshPlaces();
  }

  refreshPlaces() {
    this.placeService.getAllPlaces().subscribe(value => {
      this.places = [...value];
      // if (this.places.length === 1) {
      //   this.router.navigate(['user', 'place', this.places[0].id]);
      // }
    }, error1 => {
      console.log(error1);
    });
  }

}
