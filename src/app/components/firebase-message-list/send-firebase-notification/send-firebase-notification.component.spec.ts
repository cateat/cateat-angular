import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendFirebaseNotificationComponent } from './send-firebase-notification.component';

describe('SendFirebaseNotificationComponent', () => {
  let component: SendFirebaseNotificationComponent;
  let fixture: ComponentFixture<SendFirebaseNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendFirebaseNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendFirebaseNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
