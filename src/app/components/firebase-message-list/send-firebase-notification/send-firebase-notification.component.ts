import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {UserService} from '../../../service/user.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {not} from 'rxjs/internal-compatibility';

@Component({
  selector: 'app-send-firebase-notification',
  templateUrl: './send-firebase-notification.component.html',
  styleUrls: ['./send-firebase-notification.component.css']
})
export class SendFirebaseNotificationComponent implements OnInit {

  constructor(private userService: UserService, private fb: FormBuilder, private route: ActivatedRoute) {
  }

  validateForm: FormGroup;
  visible = false;
  isButtonLoading = false;
  @Output() onRefresh = new EventEmitter<any>();

  ngOnInit() {
    this.validateForm = this.fb.group({
      title: [null, [Validators.required]],
      content: [null, [Validators.required]],
      level: [null, [Validators.required]]
    });
  }

  open(): void {
    this.visible = true;
  }

  close(): void {
    this.visible = false;
  }

  addAnnouncement() {
    for (const i in this.validateForm.controls) {
      if (i !== null) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }
    }
    if (this.validateForm.valid) {
      this.isButtonLoading = true;
      const title = this.validateForm.get('title').value;
      const content = this.validateForm.get('content').value;
      const level = this.validateForm.get('level').value;
      const placeId = this.route.snapshot.params.placeId;
      this.userService.sendFirebaseNotification(placeId, title, level, content)
        .subscribe(value => {
          this.isButtonLoading = false;
          this.close();
          this.onRefresh.emit({});
        }, error1 => {
          console.log(error1);
        });
    }
  }
}
