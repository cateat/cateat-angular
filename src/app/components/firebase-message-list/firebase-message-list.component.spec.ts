import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirebaseMessageListComponent } from './firebase-message-list.component';

describe('FirebaseMessageListComponent', () => {
  let component: FirebaseMessageListComponent;
  let fixture: ComponentFixture<FirebaseMessageListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirebaseMessageListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirebaseMessageListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
