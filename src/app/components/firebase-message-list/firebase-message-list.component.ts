import {Component, OnInit} from '@angular/core';
import {FirebaseMessageDto} from '../../model/firebase-message-dto';
import {UserService} from '../../service/user.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-firebase-message-list',
  templateUrl: './firebase-message-list.component.html',
  styleUrls: ['./firebase-message-list.component.css']
})
export class FirebaseMessageListComponent implements OnInit {

  dataList: FirebaseMessageDto[] = [];

  constructor(private userService: UserService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.refreshMessages();
  }

  refreshMessages() {
    const placeId = this.route.snapshot.params.placeId;
    this.userService.getFirebaseMessages(placeId)
      .subscribe((value: FirebaseMessageDto[]) => {
        console.log('Result announcement');
        console.log(value);
        this.dataList = [...value];
      }, error1 => {
        console.log(error1);
      });
  }

}
