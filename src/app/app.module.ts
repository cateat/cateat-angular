import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NgZorroAntdModule, NZ_I18N, pl_PL} from 'ng-zorro-antd';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {registerLocaleData} from '@angular/common';
import pl from '@angular/common/locales/pl';
import {MainScreenComponent} from './components/main-screen/main-screen.component';
import {FirebaseMessageListComponent} from './components/firebase-message-list/firebase-message-list.component';
import {MenuElementListComponent} from './components/menu-element-list/menu-element-list.component';
import {AnnouncementListComponent} from './components/announcement-list/announcement-list.component';
import {MemberListComponent} from './components/member-list/member-list.component';
import {PlacePageComponent} from './components/place-page/place-page.component';
import {ServedMealsListComponent} from './components/served-meals-list/served-meals-list.component';
import {MenuCategoryListComponent} from './components/menu-category-list/menu-category-list.component';
import {PlacesListComponent} from './components/places-list/places-list.component';
import {PaymentsComponent} from './components/payments/payments.component';
import {ProfileComponent} from './components/profile/profile.component';
import {LoggedHeaderComponent} from './components/logged-header/logged-header.component';
import {LoginFormComponent} from './components/login-form/login-form.component';
import {PlaceSiderMenuComponent} from './components/place-sider-menu/place-sider-menu.component';
import {AnnouncementEditDrawerComponent} from './components/announcement-list/announcement-edit-drawer/announcement-edit-drawer.component';
import {BreadcrumbBarComponent} from './components/breadcrumb-bar/breadcrumb-bar.component';
import {SendFirebaseNotificationComponent} from './components/firebase-message-list/send-firebase-notification/send-firebase-notification.component';
import {AuthInterceptor} from './service/auth/auth-interceptor';
import {EditMenuElementComponent} from './components/menu-element-list/edit-menu-element/edit-menu-element.component';
import {JoinToPlaceDialogComponent} from './components/places-list/join-to-place-dialog/join-to-place-dialog.component';
import {CategoryEditComponent} from './components/menu-category-list/category-edit/category-edit.component';
import {ServedMealEditComponent} from './components/served-meals-list/served-meal-edit/served-meal-edit.component';
import {AgmCoreModule} from '@agm/core';
import {MapsConfiguration} from './configuration/MapsConfiguration';
import { DataRowComponent } from './components/place-page/data-row/data-row.component';
import { PlaceEditComponent } from './components/shared/place-edit/place-edit.component';

registerLocaleData(pl);

@NgModule({
  declarations: [
    AppComponent,
    MainScreenComponent,
    FirebaseMessageListComponent,
    MenuElementListComponent,
    AnnouncementListComponent,
    MemberListComponent,
    PlacePageComponent,
    ServedMealsListComponent,
    MenuCategoryListComponent,
    PlacesListComponent,
    PaymentsComponent,
    ProfileComponent,
    LoggedHeaderComponent,
    LoginFormComponent,
    PlaceSiderMenuComponent,
    AnnouncementEditDrawerComponent,
    BreadcrumbBarComponent,
    SendFirebaseNotificationComponent,
    EditMenuElementComponent,
    JoinToPlaceDialogComponent,
    CategoryEditComponent,
    ServedMealEditComponent,
    DataRowComponent,
    PlaceEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgZorroAntdModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: MapsConfiguration.API_KEY
    })
  ],
  providers: [{provide: NZ_I18N, useValue: pl_PL}, {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
