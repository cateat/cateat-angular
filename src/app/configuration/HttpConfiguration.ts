import {environment} from '../../environments/environment';

export class HttpConfiguration {
  public static readonly HOST = environment.host;
}
