export class MenuElementDto {

  constructor(public id: string,
              public name: string,
              public description: string,
              public price: number,
              public deleted: boolean,
              public allergens: string[]) {
  }

  public static convert(obj: any): MenuElementDto {
    console.log(obj);
    return new MenuElementDto(obj.id, obj.name, obj.description, obj.price, obj.deleted, obj.allergens);
  }

}
