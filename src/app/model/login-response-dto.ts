export class LoginResponseDto {

  constructor(
    public access_token: string,
    public token_type: string,
    public refresh_token: string,
    public expires_in: number,
    public scope: string) {
  }

  static convert(obj: any): LoginResponseDto {
    return new LoginResponseDto(obj.access_token, obj.token_type, obj.refresh_token, obj.expires_in, obj.scope);
  }

}
