export class AddressDto {
  constructor(
    public street: string,
    public number: string,
    public city: string,
    public postalCode: string,
    public latitude: number,
    public longitude: number
  ) {
  }

  static convert(obj: any): AddressDto {
    return new AddressDto(obj.street, obj.number, obj.city, obj.postalCode, obj.latitude, obj.longitude);
  }

  prettyString(): string {
    return 'ul. ' + this.street + ' ' + this.number + ', ' + this.postalCode + ' ' + this.city;
  }

}
