export class UserDto {
  constructor(
    public id: string,
    public email: string,
    public firstName: string,
    public lastName: string
  ) {
  }

  static convert(obj: any): UserDto {
    return new UserDto(obj.id, obj.email, obj.firstName, obj.lastName);
  }

}
