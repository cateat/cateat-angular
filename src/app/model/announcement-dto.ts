export class AnnouncementDto {

  constructor(
    public id: string,
    public name: string,
    public description: string,
    public created: Date,
    public isActive: boolean,
    public lastUpdate: Date
  ) {
  }

  static convert(obj: any): AnnouncementDto {
    return new AnnouncementDto(obj.id, obj.name, obj.description, obj.created, obj.active, obj.lastUpdate);
  }

}
