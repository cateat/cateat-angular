export class PlaceJoinCodeDto {

  constructor(public code: string) {
  }

  static convert(obj: any): PlaceJoinCodeDto {
    return new PlaceJoinCodeDto(obj.code);
  }

}
