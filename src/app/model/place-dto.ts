import {AddressDto} from './address-dto';

export class PlaceDto {

  constructor(
    public id: string,
    public name: string,
    public description: string,
    public allergenStrategy: string,
    public active: boolean,
    public address: AddressDto,
    public phoneNumber: string,
    public email: string,
    public website: string,
    public facebookPage: string,
    public twitterPage: string,
    public pysznePlPage: string,
    public pizzaPortalPage: string
  ) {
  }

  static convert(obj: any): PlaceDto {
    return new PlaceDto(obj.id, obj.name, obj.description, obj.allergenStrategy, obj.active, AddressDto.convert(obj.address),
      obj.phoneNumber, obj.email, obj.website, obj.facebookPage, obj.twitterPage, obj.pysznePlPage, obj.pizzaPortalPage);
  }

  getPrettyAllergenStrategy() {
    return this.allergenStrategy === 'IN_MENU' ? 'W menu' : 'Zapytaj w restauracji';
  }

}
