export class FirebaseMessageDto {
  constructor(
    public id: string,
    public title: string,
    public content: string,
    public level: string,
    public created: string) {
  }

  static convert(obj: any): FirebaseMessageDto {
    return new FirebaseMessageDto(obj.id, obj.title, obj.content, obj.level, obj.created);
  }
}
