export class MenuCategoryDto {

  constructor(
    public id: string,
    public name: string,
    public description: string,
    public deleted: boolean,
    public active: boolean
  ) {
  }

  public static convert(obj: any): MenuCategoryDto {
    return new MenuCategoryDto(obj.id, obj.name, obj.description, obj.deleted, obj.active);
  }

}
