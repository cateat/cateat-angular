import {MenuCategoryDto} from './menu-category-dto';
import {MenuElementDto} from './menu-element-dto';

export class ServedMealDto {
  constructor(
    public id: string,
    public date: Date,
    public ends: Date,
    public mealCategory: MenuCategoryDto,
    public menuElements: MenuElementDto[]
  ) {
  }

  static convert(obj: any): ServedMealDto {
    return new ServedMealDto(obj.id, obj.date, obj.ends,
      MenuCategoryDto.convert(obj.mealCategory),
      obj.menuElements.map(value => MenuElementDto.convert(value)));
  }

}
